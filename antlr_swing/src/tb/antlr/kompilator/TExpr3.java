// $ANTLR 3.4 /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g 2020-03-24 15:06:40

 package tb.antlr.kompilator;
 

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.stringtemplate.*;
import org.antlr.stringtemplate.language.*;
import java.util.HashMap;
@SuppressWarnings({"all", "warnings", "unchecked"})
public class TExpr3 extends TreeParserTmpl {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DIV", "ID", "INT", "LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "RP", "VAR", "WS"
    };

    public static final int EOF=-1;
    public static final int DIV=4;
    public static final int ID=5;
    public static final int INT=6;
    public static final int LP=7;
    public static final int MINUS=8;
    public static final int MUL=9;
    public static final int NL=10;
    public static final int PLUS=11;
    public static final int PODST=12;
    public static final int RP=13;
    public static final int VAR=14;
    public static final int WS=15;

    // delegates
    public TreeParserTmpl[] getDelegates() {
        return new TreeParserTmpl[] {};
    }

    // delegators


    public TExpr3(TreeNodeStream input) {
        this(input, new RecognizerSharedState());
    }
    public TExpr3(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected StringTemplateGroup templateLib =
  new StringTemplateGroup("TExpr3Templates", AngleBracketTemplateLexer.class);

public void setTemplateLib(StringTemplateGroup templateLib) {
  this.templateLib = templateLib;
}
public StringTemplateGroup getTemplateLib() {
  return templateLib;
}
/** allows convenient multi-value initialization:
 *  "new STAttrMap().put(...).put(...)"
 */
public static class STAttrMap extends HashMap {
  public STAttrMap put(String attrName, Object value) {
    super.put(attrName, value);
    return this;
  }
  public STAttrMap put(String attrName, int value) {
    super.put(attrName, new Integer(value));
    return this;
  }
}
    public String[] getTokenNames() { return TExpr3.tokenNames; }
    public String getGrammarFileName() { return "/home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g"; }



       Integer numer = 0;
       String ifLabel = "if_label_"+0;



     

    public static class prog_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };


    // $ANTLR start "prog"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:27:1: prog : (e+= expr |d+= decl )* -> template(name=$edeklaracje=$d) \"<deklaracje> start: <name;separator=\" \\n\"> \";
    public final TExpr3.prog_return prog() throws RecognitionException {
        TExpr3.prog_return retval = new TExpr3.prog_return();
        retval.start = input.LT(1);


        List list_e=null;
        List list_d=null;
        RuleReturnScope e = null;
        RuleReturnScope d = null;
        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:27:9: ( (e+= expr |d+= decl )* -> template(name=$edeklaracje=$d) \"<deklaracje> start: <name;separator=\" \\n\"> \")
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:27:11: (e+= expr |d+= decl )*
            {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:27:11: (e+= expr |d+= decl )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= DIV && LA1_0 <= INT)||(LA1_0 >= MINUS && LA1_0 <= MUL)||(LA1_0 >= PLUS && LA1_0 <= PODST)) ) {
                    alt1=1;
                }
                else if ( (LA1_0==VAR) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:27:12: e+= expr
            	    {
            	    pushFollow(FOLLOW_expr_in_prog81);
            	    e=expr();

            	    state._fsp--;

            	    if (list_e==null) list_e=new ArrayList();
            	    list_e.add(e.getTemplate());


            	    }
            	    break;
            	case 2 :
            	    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:27:22: d+= decl
            	    {
            	    pushFollow(FOLLOW_decl_in_prog87);
            	    d=decl();

            	    state._fsp--;

            	    if (list_d==null) list_d=new ArrayList();
            	    list_d.add(d.getTemplate());


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            // TEMPLATE REWRITE
            // 27:32: -> template(name=$edeklaracje=$d) \"<deklaracje> start: <name;separator=\" \\n\"> \"
            {
                retval.st = new StringTemplate(templateLib, "<deklaracje> start: <name;separator=\" \\n\"> ",new STAttrMap().put("name", list_e).put("deklaracje", list_d));
            }



            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "prog"


    public static class decl_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };


    // $ANTLR start "decl"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:32:2: decl : ^( VAR i1= ID ) -> dek(n=$ID.text);
    public final TExpr3.decl_return decl() throws RecognitionException {
        TExpr3.decl_return retval = new TExpr3.decl_return();
        retval.start = input.LT(1);


        CommonTree i1=null;

        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:32:8: ( ^( VAR i1= ID ) -> dek(n=$ID.text))
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:33:10: ^( VAR i1= ID )
            {
            match(input,VAR,FOLLOW_VAR_in_decl128); 

            match(input, Token.DOWN, null); 
            i1=(CommonTree)match(input,ID,FOLLOW_ID_in_decl132); 

            match(input, Token.UP, null); 


            globals.newSymbol((i1!=null?i1.getText():null));

            // TEMPLATE REWRITE
            // 33:54: -> dek(n=$ID.text)
            {
                retval.st = templateLib.getInstanceOf("dek",new STAttrMap().put("n", (i1!=null?i1.getText():null)));
            }



            }

        }
        catch (RuntimeException ex) {
            errorID(ex,i1);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "decl"


    public static class expr_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };


    // $ANTLR start "expr"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:41:1: expr : ( ^( PLUS e1= expr e2= expr ) -> add(p1=$e1.stp2=$e2.st)| ^( MINUS e1= expr e2= expr ) -> sub(p1=$e1.stp2=$e2.st)| ^( MUL e1= expr e2= expr ) -> mul(p1=$e1.stp2=$e2.st)| ^( DIV e1= expr e2= expr ) -> div(p1=$e1.stp2=$e2.st)| ^( PODST i1= ID e2= expr ) {...}? -> podst(p1=$i1.textp2=$e2.st)| INT -> int(i=$INT.textj=numer.toString())| ID {...}? -> id(p1=$ID.text));
    public final TExpr3.expr_return expr() throws RecognitionException {
        TExpr3.expr_return retval = new TExpr3.expr_return();
        retval.start = input.LT(1);


        CommonTree i1=null;
        CommonTree INT1=null;
        CommonTree ID2=null;
        TExpr3.expr_return e1 =null;

        TExpr3.expr_return e2 =null;


        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:41:9: ( ^( PLUS e1= expr e2= expr ) -> add(p1=$e1.stp2=$e2.st)| ^( MINUS e1= expr e2= expr ) -> sub(p1=$e1.stp2=$e2.st)| ^( MUL e1= expr e2= expr ) -> mul(p1=$e1.stp2=$e2.st)| ^( DIV e1= expr e2= expr ) -> div(p1=$e1.stp2=$e2.st)| ^( PODST i1= ID e2= expr ) {...}? -> podst(p1=$i1.textp2=$e2.st)| INT -> int(i=$INT.textj=numer.toString())| ID {...}? -> id(p1=$ID.text))
            int alt2=7;
            switch ( input.LA(1) ) {
            case PLUS:
                {
                alt2=1;
                }
                break;
            case MINUS:
                {
                alt2=2;
                }
                break;
            case MUL:
                {
                alt2=3;
                }
                break;
            case DIV:
                {
                alt2=4;
                }
                break;
            case PODST:
                {
                alt2=5;
                }
                break;
            case INT:
                {
                alt2=6;
                }
                break;
            case ID:
                {
                alt2=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:41:11: ^( PLUS e1= expr e2= expr )
                    {
                    match(input,PLUS,FOLLOW_PLUS_in_expr177); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr182);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr186);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    // TEMPLATE REWRITE
                    // 41:36: -> add(p1=$e1.stp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("add",new STAttrMap().put("p1", (e1!=null?e1.st:null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 2 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:42:11: ^( MINUS e1= expr e2= expr )
                    {
                    match(input,MINUS,FOLLOW_MINUS_in_expr213); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr217);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr221);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    // TEMPLATE REWRITE
                    // 42:36: -> sub(p1=$e1.stp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("sub",new STAttrMap().put("p1", (e1!=null?e1.st:null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 3 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:43:11: ^( MUL e1= expr e2= expr )
                    {
                    match(input,MUL,FOLLOW_MUL_in_expr248); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr254);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr258);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    // TEMPLATE REWRITE
                    // 43:36: -> mul(p1=$e1.stp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("mul",new STAttrMap().put("p1", (e1!=null?e1.st:null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 4 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:44:11: ^( DIV e1= expr e2= expr )
                    {
                    match(input,DIV,FOLLOW_DIV_in_expr285); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr291);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr295);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    // TEMPLATE REWRITE
                    // 44:36: -> div(p1=$e1.stp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("div",new STAttrMap().put("p1", (e1!=null?e1.st:null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 5 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:45:11: ^( PODST i1= ID e2= expr ) {...}?
                    {
                    match(input,PODST,FOLLOW_PODST_in_expr322); 

                    match(input, Token.DOWN, null); 
                    i1=(CommonTree)match(input,ID,FOLLOW_ID_in_expr326); 

                    pushFollow(FOLLOW_expr_in_expr332);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    if ( !((globals.hasSymbol((i1!=null?i1.getText():null)))) ) {
                        throw new FailedPredicateException(input, "expr", "globals.hasSymbol($ID.text)");
                    }

                    // TEMPLATE REWRITE
                    // 45:67: -> podst(p1=$i1.textp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("podst",new STAttrMap().put("p1", (i1!=null?i1.getText():null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 6 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:46:11: INT
                    {
                    INT1=(CommonTree)match(input,INT,FOLLOW_INT_in_expr360); 

                    numer++;

                    // TEMPLATE REWRITE
                    // 46:67: -> int(i=$INT.textj=numer.toString())
                    {
                        retval.st = templateLib.getInstanceOf("int",new STAttrMap().put("i", (INT1!=null?INT1.getText():null)).put("j", numer.toString()));
                    }



                    }
                    break;
                case 7 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:47:11: ID {...}?
                    {
                    ID2=(CommonTree)match(input,ID,FOLLOW_ID_in_expr428); 

                    if ( !((globals.hasSymbol((ID2!=null?ID2.getText():null)))) ) {
                        throw new FailedPredicateException(input, "expr", "globals.hasSymbol($ID.text)");
                    }

                    // TEMPLATE REWRITE
                    // 47:67: -> id(p1=$ID.text)
                    {
                        retval.st = templateLib.getInstanceOf("id",new STAttrMap().put("p1", (ID2!=null?ID2.getText():null)));
                    }



                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"

    // Delegated rules


 

    public static final BitSet FOLLOW_expr_in_prog81 = new BitSet(new long[]{0x0000000000005B72L});
    public static final BitSet FOLLOW_decl_in_prog87 = new BitSet(new long[]{0x0000000000005B72L});
    public static final BitSet FOLLOW_VAR_in_decl128 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ID_in_decl132 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PLUS_in_expr177 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr182 = new BitSet(new long[]{0x0000000000001B70L});
    public static final BitSet FOLLOW_expr_in_expr186 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MINUS_in_expr213 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr217 = new BitSet(new long[]{0x0000000000001B70L});
    public static final BitSet FOLLOW_expr_in_expr221 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MUL_in_expr248 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr254 = new BitSet(new long[]{0x0000000000001B70L});
    public static final BitSet FOLLOW_expr_in_expr258 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_DIV_in_expr285 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr291 = new BitSet(new long[]{0x0000000000001B70L});
    public static final BitSet FOLLOW_expr_in_expr295 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PODST_in_expr322 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ID_in_expr326 = new BitSet(new long[]{0x0000000000001B70L});
    public static final BitSet FOLLOW_expr_in_expr332 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_INT_in_expr360 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_expr428 = new BitSet(new long[]{0x0000000000000002L});

}