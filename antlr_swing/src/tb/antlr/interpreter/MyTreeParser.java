package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	
	protected Integer add(Integer a, Integer b) {
		return a+b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a-b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a*b;
	}
	
	protected Integer div(Integer a, Integer b) {
		if(b==0) {
			System.err.println("Dzielenie przez 0");
			return 0;
		}
		return a/b;
	}
	
	protected Integer podst(String name, Integer value) {
		
		if(globalSymbols.hasSymbol(name) == false) {
			globalSymbols.newSymbol(name);
		}
		globalSymbols.setSymbol(name, value);
		return value;
	}
	
	protected void decl(String name) {
		globalSymbols.newSymbol(name);

	}
	
	protected Integer getSymbol(String name) {
		return (int)globalSymbols.getSymbol(name);
	}
}
