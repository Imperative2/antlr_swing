// $ANTLR 3.4 /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g 2020-03-24 15:06:39

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class ExprParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DIV", "ID", "INT", "LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "RP", "VAR", "WS"
    };

    public static final int EOF=-1;
    public static final int DIV=4;
    public static final int ID=5;
    public static final int INT=6;
    public static final int LP=7;
    public static final int MINUS=8;
    public static final int MUL=9;
    public static final int NL=10;
    public static final int PLUS=11;
    public static final int PODST=12;
    public static final int RP=13;
    public static final int VAR=14;
    public static final int WS=15;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public ExprParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public ExprParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return ExprParser.tokenNames; }
    public String getGrammarFileName() { return "/home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g"; }


    public static class prog_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "prog"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:16:1: prog : ( stat )+ EOF !;
    public final ExprParser.prog_return prog() throws RecognitionException {
        ExprParser.prog_return retval = new ExprParser.prog_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF2=null;
        ExprParser.stat_return stat1 =null;


        CommonTree EOF2_tree=null;

        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:5: ( ( stat )+ EOF !)
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat )+ EOF !
            {
            root_0 = (CommonTree)adaptor.nil();


            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= ID && LA1_0 <= LP)||LA1_0==NL||LA1_0==VAR) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:8: stat
            	    {
            	    pushFollow(FOLLOW_stat_in_prog49);
            	    stat1=stat();

            	    state._fsp--;

            	    adaptor.addChild(root_0, stat1.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_prog54); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "prog"


    public static class stat_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "stat"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:20:1: stat : ( expr NL -> expr | VAR ID ( PODST expr ) NL -> ^( VAR ID ) ( ^( PODST ID expr ) )? | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | NL ->);
    public final ExprParser.stat_return stat() throws RecognitionException {
        ExprParser.stat_return retval = new ExprParser.stat_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token NL4=null;
        Token VAR5=null;
        Token ID6=null;
        Token PODST7=null;
        Token NL9=null;
        Token VAR10=null;
        Token ID11=null;
        Token NL12=null;
        Token ID13=null;
        Token PODST14=null;
        Token NL16=null;
        Token NL17=null;
        ExprParser.expr_return expr3 =null;

        ExprParser.expr_return expr8 =null;

        ExprParser.expr_return expr15 =null;


        CommonTree NL4_tree=null;
        CommonTree VAR5_tree=null;
        CommonTree ID6_tree=null;
        CommonTree PODST7_tree=null;
        CommonTree NL9_tree=null;
        CommonTree VAR10_tree=null;
        CommonTree ID11_tree=null;
        CommonTree NL12_tree=null;
        CommonTree ID13_tree=null;
        CommonTree PODST14_tree=null;
        CommonTree NL16_tree=null;
        CommonTree NL17_tree=null;
        RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
        RewriteRuleTokenStream stream_PODST=new RewriteRuleTokenStream(adaptor,"token PODST");
        RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
        RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
        RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:21:5: ( expr NL -> expr | VAR ID ( PODST expr ) NL -> ^( VAR ID ) ( ^( PODST ID expr ) )? | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | NL ->)
            int alt2=5;
            switch ( input.LA(1) ) {
            case INT:
            case LP:
                {
                alt2=1;
                }
                break;
            case ID:
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2==PODST) ) {
                    alt2=4;
                }
                else if ( (LA2_2==DIV||(LA2_2 >= MINUS && LA2_2 <= PLUS)) ) {
                    alt2=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;

                }
                }
                break;
            case VAR:
                {
                int LA2_3 = input.LA(2);

                if ( (LA2_3==ID) ) {
                    int LA2_6 = input.LA(3);

                    if ( (LA2_6==NL) ) {
                        alt2=3;
                    }
                    else if ( (LA2_6==PODST) ) {
                        alt2=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 6, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 3, input);

                    throw nvae;

                }
                }
                break;
            case NL:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:21:7: expr NL
                    {
                    pushFollow(FOLLOW_expr_in_stat71);
                    expr3=expr();

                    state._fsp--;

                    stream_expr.add(expr3.getTree());

                    NL4=(Token)match(input,NL,FOLLOW_NL_in_stat73);  
                    stream_NL.add(NL4);


                    // AST REWRITE
                    // elements: expr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 21:15: -> expr
                    {
                        adaptor.addChild(root_0, stream_expr.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:7: VAR ID ( PODST expr ) NL
                    {
                    VAR5=(Token)match(input,VAR,FOLLOW_VAR_in_stat86);  
                    stream_VAR.add(VAR5);


                    ID6=(Token)match(input,ID,FOLLOW_ID_in_stat88);  
                    stream_ID.add(ID6);


                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:14: ( PODST expr )
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:15: PODST expr
                    {
                    PODST7=(Token)match(input,PODST,FOLLOW_PODST_in_stat91);  
                    stream_PODST.add(PODST7);


                    pushFollow(FOLLOW_expr_in_stat93);
                    expr8=expr();

                    state._fsp--;

                    stream_expr.add(expr8.getTree());

                    }


                    NL9=(Token)match(input,NL,FOLLOW_NL_in_stat96);  
                    stream_NL.add(NL9);


                    // AST REWRITE
                    // elements: ID, ID, VAR, expr, PODST
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 23:30: -> ^( VAR ID ) ( ^( PODST ID expr ) )?
                    {
                        // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:33: ^( VAR ID )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_VAR.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                        // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:43: ( ^( PODST ID expr ) )?
                        if ( stream_ID.hasNext()||stream_expr.hasNext()||stream_PODST.hasNext() ) {
                            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:43: ^( PODST ID expr )
                            {
                            CommonTree root_1 = (CommonTree)adaptor.nil();
                            root_1 = (CommonTree)adaptor.becomeRoot(
                            stream_PODST.nextNode()
                            , root_1);

                            adaptor.addChild(root_1, 
                            stream_ID.nextNode()
                            );

                            adaptor.addChild(root_1, stream_expr.nextTree());

                            adaptor.addChild(root_0, root_1);
                            }

                        }
                        stream_ID.reset();
                        stream_expr.reset();
                        stream_PODST.reset();

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:24:7: VAR ID NL
                    {
                    VAR10=(Token)match(input,VAR,FOLLOW_VAR_in_stat121);  
                    stream_VAR.add(VAR10);


                    ID11=(Token)match(input,ID,FOLLOW_ID_in_stat123);  
                    stream_ID.add(ID11);


                    NL12=(Token)match(input,NL,FOLLOW_NL_in_stat125);  
                    stream_NL.add(NL12);


                    // AST REWRITE
                    // elements: ID, VAR
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 24:17: -> ^( VAR ID )
                    {
                        // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:24:20: ^( VAR ID )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_VAR.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 4 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:7: ID PODST expr NL
                    {
                    ID13=(Token)match(input,ID,FOLLOW_ID_in_stat141);  
                    stream_ID.add(ID13);


                    PODST14=(Token)match(input,PODST,FOLLOW_PODST_in_stat143);  
                    stream_PODST.add(PODST14);


                    pushFollow(FOLLOW_expr_in_stat145);
                    expr15=expr();

                    state._fsp--;

                    stream_expr.add(expr15.getTree());

                    NL16=(Token)match(input,NL,FOLLOW_NL_in_stat147);  
                    stream_NL.add(NL16);


                    // AST REWRITE
                    // elements: PODST, ID, expr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 25:24: -> ^( PODST ID expr )
                    {
                        // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:27: ^( PODST ID expr )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_PODST.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_1, stream_expr.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 5 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:26:7: NL
                    {
                    NL17=(Token)match(input,NL,FOLLOW_NL_in_stat165);  
                    stream_NL.add(NL17);


                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 26:10: ->
                    {
                        root_0 = null;
                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "stat"


    public static class expr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expr"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:31:1: expr : multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* ;
    public final ExprParser.expr_return expr() throws RecognitionException {
        ExprParser.expr_return retval = new ExprParser.expr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token PLUS19=null;
        Token MINUS21=null;
        ExprParser.multExpr_return multExpr18 =null;

        ExprParser.multExpr_return multExpr20 =null;

        ExprParser.multExpr_return multExpr22 =null;


        CommonTree PLUS19_tree=null;
        CommonTree MINUS21_tree=null;

        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:5: ( multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* )
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:7: multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_multExpr_in_expr190);
            multExpr18=multExpr();

            state._fsp--;

            adaptor.addChild(root_0, multExpr18.getTree());

            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:33:7: ( PLUS ^ multExpr | MINUS ^ multExpr )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==PLUS) ) {
                    alt3=1;
                }
                else if ( (LA3_0==MINUS) ) {
                    alt3=2;
                }


                switch (alt3) {
            	case 1 :
            	    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:33:9: PLUS ^ multExpr
            	    {
            	    PLUS19=(Token)match(input,PLUS,FOLLOW_PLUS_in_expr200); 
            	    PLUS19_tree = 
            	    (CommonTree)adaptor.create(PLUS19)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(PLUS19_tree, root_0);


            	    pushFollow(FOLLOW_multExpr_in_expr203);
            	    multExpr20=multExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multExpr20.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:34:9: MINUS ^ multExpr
            	    {
            	    MINUS21=(Token)match(input,MINUS,FOLLOW_MINUS_in_expr213); 
            	    MINUS21_tree = 
            	    (CommonTree)adaptor.create(MINUS21)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(MINUS21_tree, root_0);


            	    pushFollow(FOLLOW_multExpr_in_expr216);
            	    multExpr22=multExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multExpr22.getTree());

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"


    public static class multExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "multExpr"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:38:1: multExpr : atom ( MUL ^ atom | DIV ^ atom )* ;
    public final ExprParser.multExpr_return multExpr() throws RecognitionException {
        ExprParser.multExpr_return retval = new ExprParser.multExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token MUL24=null;
        Token DIV26=null;
        ExprParser.atom_return atom23 =null;

        ExprParser.atom_return atom25 =null;

        ExprParser.atom_return atom27 =null;


        CommonTree MUL24_tree=null;
        CommonTree DIV26_tree=null;

        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:5: ( atom ( MUL ^ atom | DIV ^ atom )* )
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:7: atom ( MUL ^ atom | DIV ^ atom )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_atom_in_multExpr242);
            atom23=atom();

            state._fsp--;

            adaptor.addChild(root_0, atom23.getTree());

            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:40:7: ( MUL ^ atom | DIV ^ atom )*
            loop4:
            do {
                int alt4=3;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==MUL) ) {
                    alt4=1;
                }
                else if ( (LA4_0==DIV) ) {
                    alt4=2;
                }


                switch (alt4) {
            	case 1 :
            	    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:40:9: MUL ^ atom
            	    {
            	    MUL24=(Token)match(input,MUL,FOLLOW_MUL_in_multExpr252); 
            	    MUL24_tree = 
            	    (CommonTree)adaptor.create(MUL24)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(MUL24_tree, root_0);


            	    pushFollow(FOLLOW_atom_in_multExpr255);
            	    atom25=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom25.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:41:9: DIV ^ atom
            	    {
            	    DIV26=(Token)match(input,DIV,FOLLOW_DIV_in_multExpr265); 
            	    DIV26_tree = 
            	    (CommonTree)adaptor.create(DIV26)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(DIV26_tree, root_0);


            	    pushFollow(FOLLOW_atom_in_multExpr268);
            	    atom27=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom27.getTree());

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "multExpr"


    public static class atom_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:45:1: atom : ( INT | ID | LP ! expr RP !);
    public final ExprParser.atom_return atom() throws RecognitionException {
        ExprParser.atom_return retval = new ExprParser.atom_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token INT28=null;
        Token ID29=null;
        Token LP30=null;
        Token RP32=null;
        ExprParser.expr_return expr31 =null;


        CommonTree INT28_tree=null;
        CommonTree ID29_tree=null;
        CommonTree LP30_tree=null;
        CommonTree RP32_tree=null;

        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:46:5: ( INT | ID | LP ! expr RP !)
            int alt5=3;
            switch ( input.LA(1) ) {
            case INT:
                {
                alt5=1;
                }
                break;
            case ID:
                {
                alt5=2;
                }
                break;
            case LP:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }

            switch (alt5) {
                case 1 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:46:7: INT
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    INT28=(Token)match(input,INT,FOLLOW_INT_in_atom294); 
                    INT28_tree = 
                    (CommonTree)adaptor.create(INT28)
                    ;
                    adaptor.addChild(root_0, INT28_tree);


                    }
                    break;
                case 2 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:47:7: ID
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    ID29=(Token)match(input,ID,FOLLOW_ID_in_atom302); 
                    ID29_tree = 
                    (CommonTree)adaptor.create(ID29)
                    ;
                    adaptor.addChild(root_0, ID29_tree);


                    }
                    break;
                case 3 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:48:7: LP ! expr RP !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    LP30=(Token)match(input,LP,FOLLOW_LP_in_atom310); 

                    pushFollow(FOLLOW_expr_in_atom313);
                    expr31=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, expr31.getTree());

                    RP32=(Token)match(input,RP,FOLLOW_RP_in_atom315); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"

    // Delegated rules


 

    public static final BitSet FOLLOW_stat_in_prog49 = new BitSet(new long[]{0x00000000000044E0L});
    public static final BitSet FOLLOW_EOF_in_prog54 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_stat71 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_NL_in_stat73 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_stat86 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_stat88 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_PODST_in_stat91 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_expr_in_stat93 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_NL_in_stat96 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_stat121 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_stat123 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_NL_in_stat125 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_stat141 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_PODST_in_stat143 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_expr_in_stat145 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_NL_in_stat147 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NL_in_stat165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_multExpr_in_expr190 = new BitSet(new long[]{0x0000000000000902L});
    public static final BitSet FOLLOW_PLUS_in_expr200 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_multExpr_in_expr203 = new BitSet(new long[]{0x0000000000000902L});
    public static final BitSet FOLLOW_MINUS_in_expr213 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_multExpr_in_expr216 = new BitSet(new long[]{0x0000000000000902L});
    public static final BitSet FOLLOW_atom_in_multExpr242 = new BitSet(new long[]{0x0000000000000212L});
    public static final BitSet FOLLOW_MUL_in_multExpr252 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_atom_in_multExpr255 = new BitSet(new long[]{0x0000000000000212L});
    public static final BitSet FOLLOW_DIV_in_multExpr265 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_atom_in_multExpr268 = new BitSet(new long[]{0x0000000000000212L});
    public static final BitSet FOLLOW_INT_in_atom294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_atom302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LP_in_atom310 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_expr_in_atom313 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_RP_in_atom315 = new BitSet(new long[]{0x0000000000000002L});

}